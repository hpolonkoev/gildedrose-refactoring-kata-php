<?php

namespace GildedRose\Items; 

/**
 * class AgedBrie
 * Extends from DefaultItem class and has his modified/overwritten updateQuality() method
 */
class AgedBrie extends DefaultItem {

     /**
     * updateQuality()
     * Update quality value, by default it increase the quality
     * 
     * @return  void
     */
    public function updateQuality() {
        if ($this->quality >= 50) return;

        $this->quality ++;
        
        $this->doubleIncreaseQuality();        
    }

    /**
     * doubleIncreaseQuality()
     * if sell_in higher or equal zero increase quality by one
     */
    private function doubleIncreaseQuality() {
        if ($this->sell_in >= 0) return;

        $this->quality ++;
    }
}