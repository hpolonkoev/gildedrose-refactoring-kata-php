<?php

namespace GildedRose\Items; 

/**
 * class ConjuredItem
 * Extends from DefaultItem class and has his modified/overwritten updateQuality() method
 */
class ConjuredItem extends DefaultItem {

    /**
     * updateQuality()
     * Update quality value, by default it decrease twice the quality
     * 
     * @return  void
     */
    public function updateQuality() {
        if ($this->quality < 1) return;
        if ($this->quality >= 50) return;

        $this->quality -=2;

        $this->doubleDecreaseQuality();                   
    }

    /**
     * doubleDecreaseQuality()
     * if sell_in lower than zero decrease quality twice
     */
    private function doubleDecreaseQuality() {
        if ($this->sell_in >= 0) return;
        
        $this->quality  -=2; 
    }
}