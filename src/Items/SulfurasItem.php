<?php

namespace GildedRose\Items; 

/**
 * class Sulfuras
 * Extends from DefaultItem class
 * Has his modified/overwritten updateQuality() and updateSellIn() methods
 */
class Sulfuras extends DefaultItem {

     /**
     * updateQuality()
     * By default doesn't modify the value
     * 
     * @return  void
     */
    public function updateQuality() {}

    /**
     * pdateSellIn()
     * By default doesn't modify the value
     * 
     * @return  void
     */
    public function updateSellIn() {}
}