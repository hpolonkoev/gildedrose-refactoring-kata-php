<?php

namespace GildedRose\Items; 

/**
 * class DefaultItem
 * Extends from Item class and has proper methods to update quality and sell_in proprieties 
 */
class DefaultItem extends Item {

    /**
     * updateQuality()
     * Update quality value, by default it decrease the quality
     * 
     * @return  void
     */
    public function updateQuality() {        
        if($this->quality < 1) return;
        if($this->quality >=50) return;
        
        $this->quality --;

        $this->doubleDescreaseQuality();
    }

    /**
     * updateSellIn()
     * Decrease sell_in value
     * 
     * @return void
     */
    public function updateSellIn() {
        $this->sell_in --;
    }

    /**
     * doubleDescreaseQuality()
     * if sell_in lower than 0 decrease quality by one
     */
    private function doubleDescreaseQuality() {
        if ($this->sell_in >= 0) return;

        $this->quality  --;    
    }
}