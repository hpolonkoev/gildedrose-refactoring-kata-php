<?php

namespace GildedRose\Items; 

/**
 * class Backstage
 * Extends from DefaultItem class and has his modified/overwritten updateQuality() method
 */
class Backstage extends DefaultItem {

     /**
     * updateQuality()
     * Update quality value, by default it increase the quality
     * 
     * @return  void
     */
    public function updateQuality() {
        if ($this->quality >= 50) return;

        if ($this->sell_in < 0) {
            $this->quality  = 0;
        } else {
            $this->quality ++;
            
            $this->increaseQuality();
        }
    }

    /**
     * increaseQuality()     * 
     * if sel_in lower than 11 but greater than 5 increase quality by one
     * if(sell_in lower than 6 increase quality by one)
     */
    private function increaseQuality() {
        if ($this->quality >= 50) return;
        if ($this->sell_in < 11) $this->quality ++;
        if ($this->sell_in < 6) $this->quality ++;
    }
}