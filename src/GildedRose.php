<?php
namespace GildedRose;

/**
 * GildedRose
 * Update items proprieties 
 */
class GildedRose {

    private $items;

    public function __construct($items) {
        $this->items = $items;
    }

    /**
     * updateQuality()
     * update item quality and sellIn  date
     */
    public function updateQuality() {
        foreach ($this->items as $item) {
            $item->updateSellIn();
            $item->updateQuality();            
        }
    }
}