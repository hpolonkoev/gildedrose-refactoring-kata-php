# GildedRose-Refactoring-Kata-PHP

Refactored **GildedRose Refactoring Kata** in PHP language

https://github.com/emilybache/GildedRose-Refactoring-Kata


## Init project

Just run `composer install` in your terminal.

## Unit tests

The PHP unit tests are in `/test` folder.

Run `vendor/bin/phpunit test/GilderRoseTest.php` 


## New specific Item

If you are looking to create a new "special" item with his own updateQuality and/or updateSellIn functions.

Just create a new class in `src/Items` extended from `DefaultItem` class and overwrite updateQuality and/or updateSellIn with your own logic.

## Info

If you create a new class or file in `scr` folder run `composer dumpautoload -o` so the class can be added to autoload.