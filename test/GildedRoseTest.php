<?php
require_once __DIR__ . '/../vendor/autoload.php';

use GildedRose\GildedRose;
use GildedRose\Items;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase {


    function testDefaultItemQuality() {
        $items = array(new Items\DefaultItem('Some item', 3, 5));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(2, $items[0]->sell_in);
        $this->assertEquals(4, $items[0]->quality);
    }

    function testDefaultItemQualityZero() {
        $items = array(new Items\DefaultItem('Some item', 3, 0));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(2, $items[0]->sell_in);
        $this->assertEquals(0, $items[0]->quality);
    }

    function testDefaultItemQualitySellInNegative() {
        $items = array(new Items\DefaultItem('Some item', 0, 5));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(-1, $items[0]->sell_in);
        $this->assertEquals(3, $items[0]->quality);
    }

    function testAgedBrieItemQuality() {
        $items = array(new Items\AgedBrie('Aged Brie', 6, 10));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(5, $items[0]->sell_in);
        $this->assertEquals(11, $items[0]->quality);
    }

    function testBackstageItemQuality() {
        $items = array(new Items\Backstage('Backstage passes to a TAFKAL80ETC concert', 15, 10));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(14, $items[0]->sell_in);
        $this->assertEquals(11, $items[0]->quality);
    }

    function testBackstageItemQualityIncreaseTwice() {
        $items = array(new Items\Backstage('Backstage passes to a TAFKAL80ETC concert', 9, 10));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(8, $items[0]->sell_in);
        $this->assertEquals(12, $items[0]->quality);
    }

    function testBackstageItemQualityIncreaseThreeTimes() {
        $items = array(new Items\Backstage('Backstage passes to a TAFKAL80ETC concert', 2, 10));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(1, $items[0]->sell_in);
        $this->assertEquals(13, $items[0]->quality);
    }

    function testBackstageItemQualitySellInNegative() {
        $items = array(new Items\Backstage('Backstage passes to a TAFKAL80ETC concert', -1, 10));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(-2, $items[0]->sell_in);
        $this->assertEquals(0, $items[0]->quality);
    }

    function testSulfurasItemQuality() {
        $items = array(new Items\Sulfuras('Sulfuras, Hand of Ragnaros', 2, 80));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(2, $items[0]->sell_in);
        $this->assertEquals(80, $items[0]->quality);
    }

    function testConjuredItemQuality() {
        $items = array(new Items\ConjuredItem('Some item', 3, 5));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(2, $items[0]->sell_in);
        $this->assertEquals(3, $items[0]->quality);
    }

    function testConjuredItemQualityZero() {
        $items = array(new Items\ConjuredItem('Some item', 3, 0));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(2, $items[0]->sell_in);
        $this->assertEquals(0, $items[0]->quality);
    }

    function testConjuredItemQualitySellInNegative() {
        $items = array(new Items\ConjuredItem('Some item', 0, 5));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(-1, $items[0]->sell_in);
        $this->assertEquals(1, $items[0]->quality);
    }

    /*/
    function testItems() {
        $items = array(
            new Item('+5 Dexterity Vest', 10, 20),
            new Item('Aged Brie', 2, 0),
            new Item('Elixir of the Mongoose', 5, 7),
            new Item('Sulfuras, Hand of Ragnaros', 0, 80),
            new Item('Sulfuras, Hand of Ragnaros', -1, 80),
            new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
            new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
            new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
            // this conjured item does not work properly yet
            new Item('Conjured Mana Cake', 3, 6)
        );
        $days = 1;
        for($i=0; $i<$days; $i++) {
            $gildedRose = new GildedRose($items);
            $gildedRose->update_quality();
            foreach($gildedRose as $kItem => $item) {
                $this->assertEquals("fixme", $items[0]->name);
            }            
        }
    }
    /**/

}
