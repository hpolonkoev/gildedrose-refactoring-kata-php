# Change Log
All changes of kata will be documented in this file

<hr><br>

## v.1.0.1 - 27/05/2019
Code review with a agile driven developer. Following this review some clean up and some code update has been applied.

New dev practice and notion learned today :

>`Guard Clause` -  is simply a check that immediately exits the function, either with a return statement or an exception. This will help to avoid nested condition and have your code shorter and more readable.


<u>__Unit Tests__</u>: should be more precisly and have only one assert condition.

### Added
- add CHANGELOG file

### Update
- update updateQuality(), updateSellIn() functions using a `Guard Clause`

### ToDo
- update Unit tests - split double asserts in different test functions instead of doing them in a single one

<br><hr><br>

## v.1.0.0 - 04/05/2019
The init  kata project with already implemented custom logic, unit tests and solution to the given task

### Added
- composer file
- add custom classes extended from Item class
- add custom functions updateQuality(), updateSellIn()
- add unit tests for each Item class
- add [README](README.md) file

### Updated
- splitted main logic from GilderRose class to seperated functions and moved them to the custom Item classes

### Remove 
- eclipse setting folder
- texttest_fixture.php 